#include <WiFi.h>

// Network credentials
const char* ssid = "HatchBack_iPhone";
const char* password = "2000MGF180"; //Will replace with VIN in future revisions

WiFiServer server(35000); //Starting Wi-Fi Server on UDP Port 35000

const unsigned long serialBlockTag = 0x11223344;

unsigned int rpm, kph, clt, oil = 0; //All RPM, Speed, Coolant Temperature and Oil Temperature start from 0.
unsigned int fuel = 0; //Starts from -20
unsigned int bat = 12; //Testing Value for now until we can establish a way to read voltage.
bool abswarn, cel, lbeam, haz, lind, rind, ebrake, bright, trunk, rfog = false; //Boolean values for indicator bulbs.

bool test = true; //Will replace with a hardware trigger in future versions

//Network Values for quick initialization instead of waiting for DHCP to kick in.
IPAddress staticIP(172, 20, 10, 6);
IPAddress gateway(172, 20, 10, 1);   
IPAddress subnet(255, 255, 255, 0);   

void setup() {
  // Initialize serial and wait for port to open
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.println("ESP32 MEMS19 REALDASH INTERFACE");
  Serial.println("Initializing...");

  // Set WiFi to station mode and connect to the access point
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  Serial.println("Connecting to ");
  Serial.print(ssid);

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }

  // Configuring static IP
  if(!WiFi.config(staticIP, gateway, subnet)) {
    Serial.println("Failed to configure Static IP");
  } else {
    Serial.println("Static IP configured!");
  }
  
  Serial.print("ESP32 IP Address: ");
  Serial.println(WiFi.localIP());  // Print the ESP32 IP address to Serial Monitor

  // Start the server
  server.begin();
  Serial.println("Server started");

  Serial.println("Setup done");
  disableCore0WDT();

  // Create tasks to run on different cores
  xTaskCreatePinnedToCore(
    WiFiServerTask,    // Function to implement the task
    "WiFiServerTask",  // Name of the task
    10000,             // Stack size in words
    NULL,              // Task input parameter
    1,                 // Priority of the task
    NULL,              // Task handle
    0);                // Core where the task should run (0 or 1)

  xTaskCreatePinnedToCore(
    CANFrameTask,      // Function to implement the task
    "CANFrameTask",    // Name of the task
    10000,             // Stack size in words
    NULL,              // Task input parameter
    1,                 // Priority of the task
    NULL,              // Task handle
    1);                // Core where the task should run (0 or 1)
}

void WiFiServerTask(void * pvParameters) {
  // Continuously handle WiFi server connections
  while (true) {
    WiFiClient client = server.available();
    if (client) {
      while (client.connected()) {
        SendCANFramesToClient(client);
      }
    }
    delay(10);
  }
}

void CANFrameTask(void * pvParameters) {
  // Continuously update CAN frame values for testing
  while (true) {
    if (test) {
      if (rpm++ > 8000) { rpm = 500; }
      if (kph++ > 230) { kph = 0; }
      if (clt++ > 130) { clt = 0;}
      if (oil++ > 170) { oil = 60; }
      if (fuel++ > 120) { fuel = 0; }
      ebrake = true;
      lind = false;
      rind = false;
      lbeam = false;
      bright = false;
      haz = true;
    }
    delay(5); // Delay for demonstration, adjust as needed
  }
}

void loop() {
  // Do nothing here, tasks are running on different cores
}

void SendCANFramesToClient(WiFiClient& client) {
  byte buf[8];

  // Build & send CAN frames to RealDash.
  // A CAN frame payload is always 8 bytes containing data in a manner
  // described by the RealDash custom channel description XML file
  // All multibyte values are handled as little endian by default.
  // Endianess of the values can be specified in XML file if it is required to use big endian values

  // Build 1st CAN frame, RPM, SPEED, CLT, TPS (just example data)
  memcpy(buf, &rpm, 2);
  memcpy(buf + 2, &kph, 2);
  memcpy(buf + 4, &clt, 2);
  memcpy(buf + 6, &oil, 2);

  // Write first CAN frame to client
  SendCANFrameToWIFIClient(client, 3200, buf);

  memcpy(buf, &fuel, 2);
  memcpy(buf + 2, &bat, 2);
  memcpy(buf + 4, &abswarn, 2);
  memcpy(buf + 6, &cel, 2);

  SendCANFrameToWIFIClient(client, 3201, buf);
  
  memcpy(buf, &lbeam, 2);
  memcpy(buf + 2, &haz, 2);
  memcpy(buf + 4, &lind, 2);
  memcpy(buf + 6, &rind, 2);

  SendCANFrameToWIFIClient(client, 3202, buf);
  
  memcpy(buf, &ebrake, 2);
  memcpy(buf + 2, &bright, 2);
  memcpy(buf + 4, &trunk, 2);
  memcpy(buf + 6, &rfog, 2);

  SendCANFrameToWIFIClient(client, 3203, buf);
}

void SendCANFrameToWIFIClient(WiFiClient& client, unsigned long canFrameId, const byte* frameData) {
  // The 4 byte identifier at the beginning of each CAN frame
  // This is required for RealDash to 'catch-up' on ongoing stream of CAN frames
  client.write((const byte*)&serialBlockTag, 4);

  // The CAN frame id number
  client.write((const byte*)&canFrameId, 4);

  // CAN frame payload
  client.write(frameData, 8);
}

void printWiFiStatus() {
  // Print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
}
