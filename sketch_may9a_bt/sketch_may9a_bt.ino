#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include <BLE2902.h>

// UUIDs for the BLE service and characteristic
#define SERVICE_UUID "6E400001-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_RX "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_TX "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"

BLEServer *pServer = NULL;
BLECharacteristic *pTxCharacteristic;
bool deviceConnected = false;
bool oldDeviceConnected = false;

const unsigned long serialBlockTag = 0x11223344;

unsigned int rpm = 0;
unsigned int kph = 0;
unsigned int clt = 0;
unsigned int oil = 60;
unsigned int fuel = 20;
unsigned int bat = 12;
bool abswarn, cel, lbeam, haz, lind, rind, ebrake, bright, trunk, rfog = false;

bool test = true;

class ServerCallbacks : public BLEServerCallbacks {
  void onConnect(BLEServer *pServer) {
    deviceConnected = true;
    Serial.println("Device connected");
  };

  void onDisconnect(BLEServer *pServer) {
    deviceConnected = false;
    Serial.println("Device disconnected");
  }
};

class CharacteristicCallbacks : public BLECharacteristicCallbacks {
  void onWrite(BLECharacteristic *pCharacteristic) {
    String rxValue = pCharacteristic->getValue();

    if (rxValue.length() > 0) {
      Serial.println("Received Value: ");
      for (char c : rxValue) {
        Serial.print(c);
      }
      Serial.println();
    }
  }
};

void setup() {
  Serial.begin(115200);

  BLEDevice::init("UARTService");
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new ServerCallbacks());
  BLEService *pService = pServer->createService(SERVICE_UUID);

  BLECharacteristic *pRxCharacteristic = pService->createCharacteristic(
    CHARACTERISTIC_UUID_RX,
    BLECharacteristic::PROPERTY_WRITE);
  pRxCharacteristic->setCallbacks(new CharacteristicCallbacks());

  pTxCharacteristic = pService->createCharacteristic(
    CHARACTERISTIC_UUID_TX,
    BLECharacteristic::PROPERTY_NOTIFY);
  pTxCharacteristic->addDescriptor(new BLE2902());

  pService->start();
  pServer->getAdvertising()->start();
  Serial.println("Waiting for a client connection to notify...");
}

void loop() {
  // Handle connection status changes
  if (deviceConnected != oldDeviceConnected) {
    if (!deviceConnected) {
      pServer->startAdvertising();
    }
    oldDeviceConnected = deviceConnected;
  }

  // Send CAN frames to client if connected
  if (deviceConnected) {
    SendCANFramesToClient();
  }

  delay(1000);  // Adjust the delay as needed
}

void SendCANFramesToClient() {
  byte buf[8];

  // Build & send CAN frames to RealDash.
  // A CAN frame payload is always 8 bytes containing data in a manner
  // described by the RealDash custom channel description XML file
  // All multibyte values are handled as little endian by default.
  // Endianess of the values can be specified in XML file if it is required to use big endian values

  // Build 1st CAN frame, RPM, SPEED, CLT, TPS (just example data)
  memcpy(buf, &rpm, 2);
  memcpy(buf + 2, &kph, 2);
  memcpy(buf + 4, &clt, 2);
  memcpy(buf + 6, &oil, 2);

  // Write first CAN frame to client
  SendCANFrameToClient(3200, buf);

  memcpy(buf, &fuel, 2);
  memcpy(buf + 2, &bat, 2);
  memcpy(buf + 4, &abswarn, 1);
  memcpy(buf + 5, &cel, 1);

  SendCANFrameToClient(3201, buf);

  memcpy(buf, &lbeam, 1);
  memcpy(buf + 1, &haz, 1);
  memcpy(buf + 2, &lind, 1);
  memcpy(buf + 3, &rind, 1);

  SendCANFrameToClient(3202, buf);

  memcpy(buf, &ebrake, 1);
  memcpy(buf + 1, &bright, 1);
  memcpy(buf + 2, &trunk, 1);
  memcpy(buf + 3, &rfog, 1);

  SendCANFrameToClient(3203, buf);

  // Test mode with dummy values
  if (test) {
    if (rpm++ > 8000) { rpm = 500; }
    if (kph++ > 230) { kph = 0; }
    if (clt++ > 130) { clt = 0;}
    if (oil++ > 170) { oil = 60; }
    fuel = 120;
    ebrake = true;
    lbeam = true;
    bright = true;
    haz = true;
  }

  delay(5); // Delay for demonstration, adjust as needed
}

void SendCANFrameToClient(unsigned long canFrameId, const byte* frameData) {
  byte buf[12];
  
  // Prepare the buffer with the serial block tag, CAN frame ID, and the frame data
  memcpy(buf, &serialBlockTag, 4);
  memcpy(buf + 4, &canFrameId, 4);
  memcpy(buf + 8, frameData, 8);
  
  // Send the data over BLE
  pTxCharacteristic->setValue(buf, sizeof(buf));
  pTxCharacteristic->notify();
}
