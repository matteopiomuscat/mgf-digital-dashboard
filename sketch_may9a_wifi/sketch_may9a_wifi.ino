#include <WiFi.h>

// WiFi credentials
char ssid[] = "MG_F";
char pass[] = "MGF180_NETWORK";

int status = WL_IDLE_STATUS;
WiFiServer server(35000);

const unsigned long serialBlockTag = 0x11223344;

// Sensor and status variables
unsigned int rpm = 0;
unsigned int kph = 0;
unsigned int clt = 0;
unsigned int oil = 60;
unsigned int fuel = 20;
unsigned int bat = 12;
bool abswarn, cel, lbeam, haz, lind, rind, ebrake, bright, trunk, rfog = false;

bool test = true;

void setup() {
  Serial.begin(9600);
  while (!Serial) {
    ; // Wait for serial port to connect. Needed for native USB port only
  }

  WiFi.mode(WIFI_AP);
  WiFi.config(IPAddress(192, 168, 4, 1), IPAddress(192, 168, 4, 1), IPAddress(255, 255, 255, 0));
  WiFi.softAP(ssid, pass, true);

  // Wait 1 second for connection:
  delay(1000);

  server.begin();
  printWiFiStatus();

  Serial.println("Setup done");

  // Create tasks for both cores
  xTaskCreatePinnedToCore(
    serverTask,        // Function to implement the task
    "serverTask",      // Name of the task
    10000,             // Stack size in words
    NULL,              // Task input parameter
    1,                 // Priority of the task
    NULL,              // Task handle
    0);                // Core where the task should run

  xTaskCreatePinnedToCore(
    logicTask,         // Function to implement the task
    "logicTask",       // Name of the task
    10000,             // Stack size in words
    NULL,              // Task input parameter
    1,                 // Priority of the task
    NULL,              // Task handle
    1);                // Core where the task should run
}

void loop() {
  // The loop function is left empty as the tasks are now running on both cores
}

// Task running on Core 0
void serverTask(void *pvParameters) {
  while (true) {
    SendCANFramesToClient();
    delay(5); // Adjust delay as needed
  }
}

// Task running on Core 1
void logicTask(void *pvParameters) {
  while (true) {
    // Update sensor data with dummy values for testing
    if (test) {
      if (rpm++ > 8000) { rpm = 500; }
      if (kph++ > 230) { kph = 0; }
      if (clt++ > 130) { clt = 0; }
      if (oil++ > 170) { oil = 60; }
      fuel = 120;
      ebrake = true;
      lbeam = true;
      bright = true;
      haz = true;
    }
    delay(100); // Adjust delay as needed
  }
}

void SendCANFramesToClient() {
  WiFiClient client = server.available();
  if (client) {
    while (client.connected()) {
      byte buf[8];

      // Build and send CAN frames
      memcpy(buf, &rpm, 2);
      memcpy(buf + 2, &kph, 2);
      memcpy(buf + 4, &clt, 2);
      memcpy(buf + 6, &oil, 2);
      SendCANFrameToClient(client, 3200, buf);

      memcpy(buf, &fuel, 2);
      memcpy(buf + 2, &bat, 2);
      memcpy(buf + 4, &abswarn, 2);
      memcpy(buf + 6, &cel, 2);
      SendCANFrameToClient(client, 3201, buf);

      memcpy(buf, &lbeam, 2);
      memcpy(buf + 2, &haz, 2);
      memcpy(buf + 4, &lind, 2);
      memcpy(buf + 6, &rind, 2);
      SendCANFrameToClient(client, 3202, buf);

      memcpy(buf, &ebrake, 2);
      memcpy(buf + 2, &bright, 2);
      memcpy(buf + 4, &trunk, 2);
      memcpy(buf + 6, &rfog, 2);
      SendCANFrameToClient(client, 3203, buf);

      delay(5); // Delay for demonstration, adjust as needed
    }
  }
}

void SendCANFrameToClient(WiFiClient& client, unsigned long canFrameId, const byte* frameData) {
  client.write((const byte*)&serialBlockTag, 4);
  client.write((const byte*)&canFrameId, 4);
  client.write(frameData, 8);
}

void printWiFiStatus() {
  IPAddress ip = WiFi.softAPIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
}
