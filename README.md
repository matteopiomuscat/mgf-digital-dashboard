# mgf-digital-dashboard

## Name
MG F Digital Dashboard (MEMS1.9 MY2000)

## Description
This project is a way to integrate a digital dash into the Rover MG F without modifying or replacing the stock ECU. An Arduino will be utilized to act as a bridge between the ECU and the device that will display the statistics. RealDash will be used as the software that displays the statistics due to its flexibility and ease of use.

## Installation

## Usage

## Support

## Roadmap
Need to figure out BT implementation on ESP32, but Wi-Fi implementation on ESP8266 is succeeding for now

## Contributing

## Authors and acknowledgment
Matteo Pio Muscat

## License
For open source projects, say how it is licensed.

## Project status
